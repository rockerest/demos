import { Component, render, html, css } from "https://unpkg.com/@thomasrandolph/taproot/lib/Component.js?module";

class MultiNotification extends Component{
	static get styles(){
		return [
			css`
			
	.notifications{
		background-color: hsla( 0, 0%, 95%, 1 );
		font-size: 16px;
		padding: 10px;
		text-align: right;
	}

	.notifications .notification{
		border: 1px solid transparent;
		border-radius: 3px;
		margin-top: 4px;
		padding: 10px;
		text-align: left;
	}

	.notifications .notification.info{
		background-color: hsla( 240, 50%, 90%, 1 );
		border-color: hsla( 240, 75%, 50%, 1 );
	}
	.notifications .notification.alert{
		background-color: hsla( 0, 50%, 90%, 1 );
		border-color: hsla( 0, 75%, 50%, 1 );
	}
	.notifications .notification.warning{
		background-color: hsla( 41.9, 75%, 90%, 1 );
		border-color: hsl( 41.9, 97.2%, 57.3% );
	}
	.notifications .notification.okay{
		background-color: hsla( 120, 50%, 90%, 1 );
		border-color: hsla( 120, 75%, 50%, 1 );
	}

	.notifications .notification button{
		float: right;
	}

	p{
		margin: 0;
		display: inline-block;
	}
			
`
		];
	}

	static get properties(){
		return {
			"notifications": { "type": Array }
		};
	}

	constructor( ...args ){
		super( ...args );

		this.notifications = [];

		this.addEventListener( "click", ( event ) => {
			let notification = event.composedPath().filter( ( node ) => node.classList?.contains( "notification" ) );

			if( notification.length ){
				notification = notification [ 0 ];
			}

			let notifications = Array.from( this.shadowRoot.querySelectorAll( ".notification" ) );
			let idx = notifications.indexOf( notification );
				
			this.notifications = this.notifications.filter( ( n, i ) => i != idx );
		} );
	}

	dismissAll(){
		this.notifications = [];
	}

	render(){
		return html`
		
<div class="notifications">
	<button @click="${this.dismissAll}">Dismiss All</button>
	${this.notifications.map( ( n ) => {
		return html`
			<div class="notification ${n.type}">
				<p>${n.text}</p>
				<button>Dismiss</button>
			</div>
		`;
	} )}
</div>
		
		`;
	}
}
customElements.define( "multi-notification", MultiNotification );

var notifications = [
	{
		"text": "This is a notification",
		"type": "info"
	},
	{
		"text": "Another notification",
		"type": "alert"
	},
	{
		"text": "Notification 3",
		"type": "warning"
	},
	{
		"text": "Something else",
		"type": "okay"
	}
]

render( html`<multi-notification .notifications="${notifications}"></multi-notification>`, document.getElementById( "multi-notification" ) );